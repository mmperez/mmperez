package es.uvigo.esei.dgss.exercises.domain;

import es.uvigo.esei.dgss.exercises.domain.Post;
import java.io.Serializable;
import java.lang.String;
import javax.persistence.*;

@Entity
@DiscriminatorValue("link")
public class Link extends Post implements Serializable {

	private String url;
	
	private static final long serialVersionUID = 1L;

	public Link() {
		super();
	}   
	
	public String getUrl() {
		return this.url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
   
}