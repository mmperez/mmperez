	package es.uvigo.esei.dgss.exercises.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.xml.bind.annotation.XmlTransient;

@Entity
public class Comment implements Serializable {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	private Date date;

	private String comment;
	
	private static final long serialVersionUID = 1L;
	
	/*** USERS SECTION ***/
	
	@ManyToOne
	@PrimaryKeyJoinColumn(name="comment_user", referencedColumnName="login")
	@XmlTransient
	private User userComment;
	@XmlTransient
	public User getUserComment() {
		return userComment;
	}
	
	/*** POSTS SECTION ***/
	
	@ManyToOne
	@PrimaryKeyJoinColumn(name="comment_post", referencedColumnName="id")
	@XmlTransient
	private Post postComment;
	@XmlTransient
	public Post getPostComment() {
		return postComment;
	}
	
	public void setUserAndPost(User user, Post post) {
		if(userComment != null && postComment != null) {
			userComment.internalRemoveComment(this);
			postComment.internalRemoveComment(this);
		}
		
		userComment = user;
		postComment = post;
		
		if(userComment != null && postComment != null) {
			userComment.internalAddComment(this);
			postComment.internalAddComment(this);
		}
	}
	
	public Comment(){
		super();
	}
	
	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}
	
	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
	
	public Post getPost() {
		return postComment;
	}

	public void setPost(Post post) {
		this.postComment = post;
	}

}