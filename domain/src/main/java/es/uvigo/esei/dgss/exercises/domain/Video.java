package es.uvigo.esei.dgss.exercises.domain;

import es.uvigo.esei.dgss.exercises.domain.Post;
import java.io.Serializable;
import javax.persistence.*;

@Entity
@DiscriminatorValue("video")
public class Video extends Post implements Serializable {
	
	protected String url;
	
	private static final long serialVersionUID = 1L;

	public Video() {
		super();
	}   
	
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	
}