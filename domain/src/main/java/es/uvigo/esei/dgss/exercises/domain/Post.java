package es.uvigo.esei.dgss.exercises.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.sql.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.xml.bind.annotation.XmlTransient;

@Entity
@Inheritance(strategy=InheritanceType.JOINED)
@DiscriminatorColumn(name="post_type")
public abstract class Post implements Serializable {


	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE)
	private int id;
	
	private Date date;
	
	private static final long serialVersionUID = 1L;
	
	public Post() {};
	
	@ManyToOne
	@XmlTransient
	private User userPost;
	@XmlTransient
	public User getUserPost() {
		return userPost;
	}
	
	public void setUserPost(User user) {
		if(this.userPost != null) {
			this.userPost.internalRemovePost(this);
		}
		this.userPost = user;
		if(user != null) {
			user.internalAddPost(this);
		}
	}
	
	/*** COMMENTS SECTION ***/
	
	@OneToMany(mappedBy="postComment",cascade=CascadeType.REMOVE)
	@XmlTransient
	private List<Comment> comments; 
	@XmlTransient
	public List<Comment> getComments() {
		return comments;
	}
	
	public void addComment(Comment comment, User user) {
		comment.setUserAndPost(user, this);
	}
	
	public void removeComment(Comment comment, User user) {
		comment.setUserAndPost(user, this);
	}
	
	public void internalAddComment(Comment comment) {
		if(comments == null)
			comments = new ArrayList<Comment>();
		comments.add(comment);
	}
	
	public void internalRemoveComment(Comment comment) {
		if(comments != null)
			comments.remove(comment);
	}
	
	/*** LIKES SECTION ***/
	
	@ManyToMany(cascade=CascadeType.REMOVE)
	@XmlTransient
	@JoinTable(
			name="Likes",
			joinColumns=@JoinColumn(name="post_id", referencedColumnName="id"),
			inverseJoinColumns=@JoinColumn(name="user_login", referencedColumnName="login"))
	private List<User> likes;
	@XmlTransient
	public List<User> getLikes(){
		if(likes == null)
			likes = new ArrayList<User>();
		return likes;
	}
	
	public void addLike(User user) {
		if(likes == null)
			likes = new ArrayList<User>();
		likes.add(user);
		user.addLikedPost(this);
	}
	
	public void removeLike(User user) {
		if(likes != null) {
			likes.remove(user);
			user.removeLikedPost(this);
		}
	}
	
	/*** GETTERS AND SETTERS SECTION ***/
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
	
}