package es.uvigo.esei.dgss.exercises.domain;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.xml.bind.annotation.XmlTransient;

@Entity
@IdClass(Friendship.FriendshipKey.class)
public class Friendship implements Serializable {

	@Id
	private String userFromLogin;
	@Id
	private String userToLogin;
	
	@ManyToOne
	@XmlTransient
	@PrimaryKeyJoinColumn(name="userFrom", referencedColumnName="login")
	private User userFrom;
	
	@ManyToOne
	@XmlTransient
	@PrimaryKeyJoinColumn(name="userTo", referencedColumnName="login")
	private User userTo;
	
	private Date date;
	
	private boolean accepted = false;
	
	private static final long serialVersionUID = 1L;

	public Friendship() {}; 
	
	public static class FriendshipKey implements Serializable {

		private static final long serialVersionUID = 1L;
		
		private String userFromLogin;
		private String userToLogin;

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((userFromLogin == null) ? 0 : userFromLogin.hashCode());
			result = prime * result + ((userToLogin == null) ? 0 : userToLogin.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			FriendshipKey other = (FriendshipKey) obj;
			if (userFromLogin == null) {
				if (other.userFromLogin != null)
					return false;
			} else if (!userFromLogin.equals(other.userFromLogin))
				return false;
			if (userToLogin == null) {
				if (other.userToLogin != null)
					return false;
			} else if (!userToLogin.equals(other.userToLogin))
				return false;
			return true;
		};
		
	}
	
	/*** GETTERS AND SETTERS SECTION ***/
	
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	@XmlTransient
	public User getUserFrom() {
		return userFrom;
	}
	
	public void setUserFrom(User userFrom) {
		this.userFrom = userFrom;
		this.userFromLogin = userFrom.getLogin();
	}
	@XmlTransient
	public User getUserTo() {
		return userTo;
	}
	
	public void setUserTo(User userTo) {
		this.userTo = userTo;
		this.userToLogin = userTo.getLogin();
	}

	public boolean isAccepted() {
		return accepted;
	}
	public void setAccepted(boolean accepted) {
		this.accepted = accepted;
	}
}
