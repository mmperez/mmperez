package es.uvigo.esei.dgss.exercises.domain;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.xml.bind.annotation.XmlTransient;

@Entity
public class User {
	
	@JoinColumn
	@Id 
	private String login;
	
	private String password,name,email,role;
	
	@Lob
	private byte[]picture;
	
	private static final long serialVersionUID = 1L;
	
	public User() { };
	
	public User(String login, String name, String password,String email, String role) {
		this.login = login;
		this.name= name;
		this.password = password;
		this.email = email;
		this.role = role;
	}
	
	/*** POSTS SECTION ***/
	
	@OneToMany(mappedBy = "userPost", orphanRemoval=true)
	@XmlTransient
	private List<Post> posts;
	@XmlTransient
	public List<Post> getPosts() {
		return posts;
	}
	
	public void addPost(Post post) {
		post.setUserPost(this);
	}
	
	public void removePost(Post post) {
		post.setUserPost(null);
	}
	
	void internalAddPost(Post post) {
		if(posts == null)
			posts = new ArrayList<Post>();
		posts.add(post);
	}
	
	void internalRemovePost(Post post) {
		if(posts!=null)
			posts.remove(post);
	}
	
	/*** COMMENTS SECTION ***/
	
	@OneToMany(mappedBy="userComment", cascade=CascadeType.ALL)
	@XmlTransient
	private List<Comment> comments;
	@XmlTransient
	public List<Comment> getComments() {
		return comments;
	}
	
	public void addComment(Comment comment, Post post) {
		comment.setUserAndPost(this, post);
	}
	
	public void removeComment(Comment comment, Post post) {
		comment.setUserAndPost(this, post);
	}
	
	public void internalAddComment(Comment comment) {
		if(comments == null)
			comments = new ArrayList<Comment>();
		comments.add(comment);
	}
	
	public void internalRemoveComment(Comment comment) {
		if(comments != null)
			comments.remove(comment);
	}
	
	/*** LIKES SECTION ***/
	
	@ManyToMany(mappedBy="likes", cascade=CascadeType.REMOVE)
	@XmlTransient
	private List<Post> likedPosts;
	
	@XmlTransient
	public List<Post> getLikedPosts() {
		if(likedPosts == null)
			likedPosts = new ArrayList<Post>();
		return likedPosts;
	}
	
	void addLikedPost(Post post) {
		if(likedPosts == null)
			likedPosts = new ArrayList<Post>();
		likedPosts.add(post);
		if(!post.getLikes().contains(this))
			post.addLike(this);
	}
	
	void removeLikedPost(Post post) {
		if(likedPosts != null) {
			likedPosts.remove(post);
			post.removeLike(this);
		}
	}
	
	/*** GETTERS AND SETTERS SECTION ***/
	
	public String getLogin() {
		return login;
	}
	
	public void setLogin(String login) {
		this.login = login;
	}
	
	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	
	public byte[] getPicture() {
		return picture;
	}
	
	public void setPicture(byte[] picture) {
		this.picture = picture;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

}