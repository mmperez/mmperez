package es.uvigo.esei.dgss.exercises.domain;

import es.uvigo.esei.dgss.exercises.domain.Post;
import java.io.Serializable;
import java.lang.String;
import javax.persistence.*;

@Entity
@DiscriminatorValue("photo")
public class Photo extends Post implements Serializable {

	private String content;
	
	private static final long serialVersionUID = 1L;

	public Photo() {
		super();
	}   
	
	public String getContent() {
		return this.content;
	}

	public void setContent(String content) {
		this.content = content;
	}
   
}
