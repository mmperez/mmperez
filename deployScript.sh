#!/bin/bash

{ #try
cwd=$(pwd)
mvn clean
mvn install
cd web/
mvn -e wildfly:deploy
cd ..
} ||
{ #catch
echo "ERROR: deployment failure"
}

cd ..
