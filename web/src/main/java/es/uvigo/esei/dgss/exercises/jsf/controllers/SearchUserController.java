package es.uvigo.esei.dgss.exercises.jsf.controllers;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import es.uvigo.esei.dgss.exercises.service.sample.*;
import es.uvigo.esei.dgss.exercises.domain.Post;
import es.uvigo.esei.dgss.exercises.domain.User;

@Named(value="searchUserController")
@SessionScoped
public class SearchUserController implements Serializable{


	private static final long serialVersionUID = 1L;

	@EJB
	private UserEJB userEJB;
	
	@EJB
	private PostEJB postEJB;
	
	private String searchText;
	private List <User> users;
	private List <Post> posts = new ArrayList<Post>();
	

	public String getSearchText() {
		return searchText;
	}

	public void setSearchText(String searchText) {
		this.searchText = searchText;
	}

	public List<User> getUsers() {
		return users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}
	
	public List <Post> getPosts() {
		return posts;
	}

	public void setPosts(List <Post> posts) {
		this.posts = posts;
	}
	
	public String redirectTo(String url) {
		return url + "?faces-redirect=true";
	}
	
	public String getViewId() {
		return FacesContext.getCurrentInstance().getViewRoot().getViewId();
	}
	
	public String doSearch(){
		
		users = userEJB.findUsers(searchText);
				
		return redirectTo(this.getViewId());
	}
	
	public String viewProfile(User user) {
		
		
		posts = postEJB.getPostsByUserLogin(user.getLogin());
		
		
		return redirectTo(this.getViewId());
	}
	
}
