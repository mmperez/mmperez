package es.uvigo.esei.dgss.exercises.web;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import es.uvigo.esei.dgss.exercises.domain.Comment;
import es.uvigo.esei.dgss.exercises.domain.Friendship;
import es.uvigo.esei.dgss.exercises.domain.Link;
import es.uvigo.esei.dgss.exercises.domain.Photo;
import es.uvigo.esei.dgss.exercises.domain.Post;
import es.uvigo.esei.dgss.exercises.domain.User;
import es.uvigo.esei.dgss.exercises.domain.Video;
import es.uvigo.esei.dgss.exercises.service.sample.UserEJB;
import es.uvigo.esei.dgss.exercises.service.sample.PostEJB;
import es.uvigo.esei.dgss.exercises.service.sample.MailEJB;

@WebServlet("/SimpleServlet")
public class SimpleServlet extends HttpServlet {

	
    @Inject
    private Facade facade;

    @Resource
    private UserTransaction transaction;
    
	@EJB
	private PostEJB postEJB;

	@EJB
	private UserEJB userEJB;
	
	@EJB
	private MailEJB mailEJB;

    
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        PrintWriter writer = resp.getWriter();

        writer.println("<html>");
        writer.println("<body>");
        writer.println("<h1>FACADE TESTS</h1>");

        try {
            transaction.begin();

            //ADDED USERS
            User u1 = facade.addUser("User1", "Marcos", "password","mail@mail.com","user");
            User u2 = facade.addUser("User2", "Paco"  , "password","mail@mail.com","admin");
            User u3 = facade.addUser("User3", "Fran"  , "password","mail@mail.com","admin");
            User u4 = facade.addUser("User4", "Miguel", "password","mail@mail.com","admin");
            User um = facade.addUser("Mail",  "Mail",   "password","mmperez@esei.uvigo.es","admin");
            
            //SEND EMAIL
            mailEJB.sendEmail(um, "Email","Email body");
            
            //SHOW USERS
            writer.write("<p>"+"User: "+u1.getLogin()+" created successfully"+"<p>");
            writer.write("<p>"+"User: "+u2.getLogin()+" created successfully"+"<p>");
            writer.write("<p>"+"User: "+u3.getLogin()+" created successfully"+"<p>");
            writer.write("<p>"+"User: "+u4.getLogin()+" created successfully"+"<p>");
            
            //NEW FRIENDSHIPS
            Friendship f1 = facade.addFriendship(u1, u2);
            Friendship f2 = facade.addFriendship(u1, u3);
            //SHOW FRIENDSHIPS
            writer.write("<p>"+"Friendship between "+f1.getUserFrom().getLogin() + " and "
            				+f1.getUserTo().getLogin() + " at " + f1.getDate().toString() + " created succesfully"+"<p>");
            writer.write("<p>"+"Friendship between "+f2.getUserFrom().getLogin() + " and "
    				+f2.getUserTo().getLogin() + " at " + f2.getDate().toString() + " created succesfully"+"<p>");
            
            //SHOW FRIENDSHIPS TO USER 1
            for(User u : facade.getAllFriends(u1)) {
            	writer.println("<p>" + u.getName()+ "<b> is friend to "+ u1.getName() +"</b></p>");
            }
            
            // CREATE A POSTS AND COMMENTS POSTS
            Link  link  = postEJB.createPostLink(u1.getLogin(),  "www.link.com");
            Video video = postEJB.createPostVideo(u2.getLogin(), "https://www.youtube.com/");
            Photo photo = postEJB.createPostPhoto(u3.getLogin(), "photo.jpg");   
            facade.addComment(u1, link,  "Post Link");
            facade.addComment(u2, video, "Post Video");
            facade.addComment(u3, photo, "Post Photo");
            
            //SHOW A POST OF MY FRIENDS
            Map<User, List<Post>> mapPostsOfFriends = facade.getAllPostsFromFriendsByUser(u1);
            writer.println("<p> Post by friends of user: " + u1.getLogin() + "</p>");
            writer.println("<ul>");
            for(User u : facade.getAllFriends(u1)) {
            	for(Post p : mapPostsOfFriends.get(u)) {
            		writer.println("<li>" + u.getName() + " <b>created this post:</b> Post id " + p.getId() + " on date " + p.getDate()+"</li>");
            	}
            }
            writer.println("</ul>");
            
            //SHOW A POST OF MY FRIENDS IS COMMENTS
            writer.println("<p> Post comments by friends of user: " + u1.getLogin() + "</p>");
            writer.println("<ul>");
            for(Post p : facade.getPostsCommentedByFriendsDate(u1, new Date(1))) {
            		writer.println("<li>Post de " + p.getUserPost().getLogin() + " con id: " + p.getId() + "</li>");
            }
            writer.println("</ul>");
            
            //CREATE LIKES IN POSTS       
            photo.addLike(u1);
            photo.addLike(u2);
            link.addLike(u3);
            link.addLike(u4);
            video.addLike(u2);
            
            //SHOW LIKES POSTS
            writer.println("<p>Friends " + u1.getLogin() + " liked post " + link.getId() + "</p>");
            writer.println("<ul>");
            for(User u : facade.getFriendsLikedMyPosts(u1, link)) {
            	writer.println("<li>" + u.getLogin() + "</li>");
            }
            writer.println("</ul>");
            
            //SHOW LIKES POST PHOTOS
            writer.println("<p>Friends " + u1.getLogin() + " liked post photo " + link.getId() + "</p>");
            writer.println("<ul>");
            for(Photo f : facade.getFriendsLikedMyPhotos(u1)) {
            	writer.println("<li> Photo id: " + f.getId() + "</li>");
            }
            writer.println("</ul>");
            
            //SHOW POTENTIAL FRIENDSHIPS
            writer.println("<p>Potential friendship to " + u1.getLogin() + "</p>");
            writer.println("<ul>");
            for(User u : facade.getPontentialFriends(u1)) {
            	writer.println("<li> Potential friendship: " + u.getName() + "</li>");
            }
            writer.println("</ul>");
            
            transaction.commit();
            
            
        } catch (
		NotSupportedException |
		SystemException |
		SecurityException |
		IllegalStateException |
		RollbackException |
		HeuristicMixedException |
		HeuristicRollbackException e) {
	        try {
	            transaction.rollback();
	        } catch (IllegalStateException e1) {
	            e1.printStackTrace();
	        } catch (SecurityException e1) {
	            e1.printStackTrace();
	        } catch (SystemException e1) {
	            e1.printStackTrace();
	        }
        }
      
        writer.println("</body>");
        writer.println("</html>");

    }
}