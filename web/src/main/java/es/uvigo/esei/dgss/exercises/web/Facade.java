package es.uvigo.esei.dgss.exercises.web;

import java.util.ArrayList;
import java.util.HashMap;
import java.sql.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.ejb.EJB;
import javax.enterprise.context.Dependent;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import es.uvigo.esei.dgss.exercises.domain.Comment;
import es.uvigo.esei.dgss.exercises.domain.Friendship;
import es.uvigo.esei.dgss.exercises.domain.Friendship.FriendshipKey;
import es.uvigo.esei.dgss.exercises.service.sample.StatisticsEJB;
import es.uvigo.esei.dgss.exercises.domain.Link;
import es.uvigo.esei.dgss.exercises.domain.Photo;
import es.uvigo.esei.dgss.exercises.domain.Post;
import es.uvigo.esei.dgss.exercises.domain.User;

@Dependent
public class Facade {
	@PersistenceContext
    private EntityManager em;
    
    @PersistenceContext
    public void setEntityManager(EntityManager em) {
        this.em = em;
    }
    
	@EJB
	private StatisticsEJB statisticsEJB;
	
    //Create a new user given its login, name, password
    //Params login, name, password
    public User addUser(String login, String name, String password,String email,String role) {
    	User user = new User(login,name,password,email,role);
        user.setName(name);
        user.setPassword(password);
        user.setRole(role);
        user.setEmail(email);
        em.persist(user);
		statisticsEJB.addTotalUser(1);
        return user;
    }   
    
    //Create a friendship between two given users
    //Params user_from, user_to
    public Friendship addFriendship(User user_from,User user_to)
    {
    	Friendship friendship = new Friendship();
    	friendship.setUserFrom(user_from);
    	friendship.setUserTo(user_to);
    	friendship.setDate(new Date(System.currentTimeMillis()));
    	
    	em.persist(friendship);
    	
    	return friendship;
    }
    
    //Get all friends of a given user
    //Params user
    @SuppressWarnings("unchecked")
	public List<User> getAllFriends(User user) {
    	List<User> friends = new ArrayList<User>();
    	
    	friends.addAll((List<User>)em.createQuery("SELECT f.userTo FROM Friendship f WHERE f.userFrom = :u").setParameter("u", user).getResultList());
    	friends.addAll((List<User>)em.createQuery("SELECT f.userFrom FROM Friendship f WHERE f.userTo = :u").setParameter("u", user).getResultList());
    	
    	return friends;
    }
    
    //Get all posts of the friends of a given user
    //Param user
	public Map<User, List<Post>> getAllPostsFromFriendsByUser(User user) {
    	Map<User, List<Post>> posts = new HashMap<User, List<Post>>();
    	
    	for(User u : getAllFriends(user)) {
    		posts.put(u, u.getPosts());
    	}
    	
    	return posts;
    }
	
	//Get the posts that have been commented by the friends of a given user after a given date
	//Params user, date
	public List<Post> getPostsCommentedByFriendsDate(User user, Date date) {
		List<Post> posts = new ArrayList<Post>();
		
		for(User u : getAllFriends(user)) {			
			for(Comment c : u.getComments()) {
				if(c.getDate().compareTo(date) > 0)
					posts.add(c.getPost());
			}
		}
		
		return posts;
	}
	
	//Create Comment
	//Params user, post, comment
	public Comment addComment(User user, Post post, String comment){
		Comment c = new Comment();
		c.setComment(comment);
		c.setDate(new Date(System.currentTimeMillis()));
		c.setUserAndPost(user, post);
		em.persist(c);		
		return c;
	}
	
	//Get the users which are friends of a given user who like a given post
	//Params user, post
	public List<User> getFriendsLikedMyPosts(User user, Post post) {
		List<User> friends = getAllFriends(user);		
		friends.retainAll(post.getLikes());
		return friends;
	}
	
	//Give me all the pictures a given user likes
	//Params user
	public List<Photo> getFriendsLikedMyPhotos(User user) {
		List<Photo> photos = new ArrayList<Photo>();
		
		for(Post p : user.getLikedPosts()) {
			if(p instanceof Photo)
				photos.add((Photo)p);
		}
		
		return photos;
	}
	
	//Create a list of potential friends for a given user
	//params user
	public List<User> getPontentialFriends(User user) {
		List<User> potentialFriends = new ArrayList<User>();
		List<User> friends = getAllFriends(user);
		return potentialFriends;
	}
}