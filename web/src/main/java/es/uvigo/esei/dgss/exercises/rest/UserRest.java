package es.uvigo.esei.dgss.exercises.rest;

import java.security.Principal;

import javax.annotation.security.PermitAll;
import javax.ejb.EJB;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import es.uvigo.esei.dgss.exercises.domain.User;
import es.uvigo.esei.dgss.exercises.service.sample.UserEJB;

@Path("/user")
@Consumes(javax.ws.rs.core.MediaType.APPLICATION_JSON)
public class UserRest {
	@Context
	private UriInfo uriInfo;

	@EJB
	private UserEJB userEJB;
	
	@Inject
	Principal principal;
	
    private String getLoggedUser() {
    	return userEJB.getSessionContext().getCallerPrincipal().getName();
    }

    //Create an user (not authenticated).
	@POST
	public Response createUser(User u) {
		userEJB.createUser(u);

		return Response.created(uriInfo.getAbsolutePathBuilder().path("/api/user" + u.getLogin()).build()).build();

	}

	@PUT
	@Path("/update/{login}")
	public Response updateUser(@PathParam("login") String login, User u) {
		u.setLogin(login);
		userEJB.updateUser(u);

		return Response.noContent().build();

	}

	@GET
	@Path("/{login}")
	@PermitAll
	@Produces(MediaType.APPLICATION_JSON)
	public Response getUser(@PathParam("login") String login) {
		User user = userEJB.findByLogin(login);
		
		return Response.ok(user).build();
	}
	
	//Request another users friendship (from authenticated user to another user).
    @POST
    @Path("/friendship/{login}")
    public Response requestFriendShip(@PathParam("login") String login) {
    	userEJB.createFriendship(getLoggedUser(),login);
    	
    	return Response.ok().build();
    }
	
    
    //Accept friendship request (made to the authenticated user).
    @PUT
    @Path("/accept/{login}")
    public Response acceptFriendship(@PathParam("login") String login) {
    	String loggedUser = getLoggedUser();
    	userEJB.acceptFriendship(login, loggedUser);
    	
    	return Response.ok().build();
    }
	
    //Get friendships requests (friendships made to the authenticated user).
    @GET
    @Path("/myrequests")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getFriendshipRequests() {
    	String loggedUser = getLoggedUser();
    	
    	return Response.ok(userEJB.getFriendshipRequest(loggedUser)).build();
    }

}