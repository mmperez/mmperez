package es.uvigo.esei.dgss.exercises.rest;

import java.security.Principal;
import java.util.List;

import javax.annotation.security.PermitAll;
import javax.ejb.EJB;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import es.uvigo.esei.dgss.exercises.domain.Link;
import es.uvigo.esei.dgss.exercises.domain.Photo;
import es.uvigo.esei.dgss.exercises.domain.Post;
import es.uvigo.esei.dgss.exercises.domain.User;
import es.uvigo.esei.dgss.exercises.domain.Video;
import es.uvigo.esei.dgss.exercises.service.sample.*;

@Path("/post")
@Consumes(javax.ws.rs.core.MediaType.APPLICATION_JSON)
public class PostRest {
	@Context
	private UriInfo uriInfo;

	@EJB
	private UserEJB userEJB;
	
	@EJB
	private PostEJB postEJB;
	
	@Inject
	Principal principal;
	
    private String getLoggedUser() {
    	return userEJB.getSessionContext().getCallerPrincipal().getName();
    }
	//Post normal text, links and photos (you can make three different functions. The author should be the authenticated user).
    @POST
    @Path("/photo/{photo}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response postPhoto(@PathParam("photo")String photo) {
    	String u = getLoggedUser();
    	Post p = postEJB.createPostPhoto(u, photo);
    	
    	return Response.created(
    			uriInfo.getAbsolutePathBuilder().path("/post?id=" +
    					p.getId()).build())
    			.build();
    }
    
    //Modify a post (only posts authored by the authenticated user can be edited).
    @PUT
    @Path("/editLink/{id}/")
    public Response editLink(@PathParam("id") int id, Photo photo) {
    	photo.setId(id);
    	postEJB.editAuthoredPost(getLoggedUser(), photo);
    	
    	return Response.ok().build();
    }
    
	//Post normal text, links and photos (you can make three different functions. The author should be the authenticated user).
    @POST
    @Path("/video/{url}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response postVideo(@PathParam("url")String url) {
    	String u = getLoggedUser();
    	Post p = postEJB.createPostVideo(u, url);
    	
    	return Response.created(
    			uriInfo.getAbsolutePathBuilder().path("/post?id=" +
    					p.getId()).build())
    			.build();
    }
    
    //Modify a post (only posts authored by the authenticated user can be edited).
    @PUT
    @Path("/editVideo/{id}/")
    public Response editVideo(@PathParam("id") int id, Video video) {
    	video.setId(id);
    	postEJB.editAuthoredPost(getLoggedUser(), video);
    	
    	return Response.ok().build();
    }
    
	//Post normal text, links and photos (you can make three different functions. The author should be the authenticated user).
    @POST
    @Path("/link/{link}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response postLink(@PathParam("link")String link) {
    	String u = getLoggedUser();
    	Post p = postEJB.createPostLink(u, link);
    	
    	return Response.created(
    			uriInfo.getAbsolutePathBuilder().path("/post?id=" +
    					p.getId()).build())
    			.build();
    }
    
    //Modify a post (only posts authored by the authenticated user can be edited).
    @PUT
    @Path("/editLink/{id}/")
    public Response editLink(@PathParam("id") int id, Link link) {
    	link.setId(id);
    	postEJB.editAuthoredPost(getLoggedUser(), link);
    	
    	return Response.ok().build();
    }
      
    //Like a given post (the authenticated user does the like).
    @POST
    @Path("/addlike/{id}")
    public Response postLink(@PathParam("id")int id) {
    	String u = getLoggedUser();
    	postEJB.addAutenticateLikes(id, u);
    	
    	return Response.ok().build();
    }
    
    @POST
    @Path("/edit/{id}")
    public Response editPost(@PathParam("id")int id) {
    	String u = getLoggedUser();
    	postEJB.addAutenticateLikes(id, u);
    	
    	return Response.ok().build();
    }
    
    @DELETE
    @Path("/delete/{id}")
    public Response deletePost(@PathParam("id") int id) {
    	postEJB.deleteAuthorPost(getLoggedUser(), id);
    	return Response.ok().build();
    }
    
    //Get my posts (the authenticated user posts).
	@GET
	@Path("myposts")
	@PermitAll
	@Produces(MediaType.APPLICATION_JSON)
	public Response getUser() {
		List<Post> posts = postEJB.getPostsByUserLogin(getLoggedUser());
		
		return Response.ok(posts).build();
	}

}