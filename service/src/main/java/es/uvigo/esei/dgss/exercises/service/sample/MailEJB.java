package es.uvigo.esei.dgss.exercises.service.sample;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.Resource;
import javax.ejb.Asynchronous;
import javax.ejb.Stateless;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import es.uvigo.esei.dgss.exercises.domain.User;

@Stateless
public class MailEJB {
	
	@Resource(name = "java:jboss/mail/gmail")
	private Session session;
	
	@Asynchronous
	public void sendEmail(User u, String subject, String body) {
		try {
			Message message = new MimeMessage(session);
			InternetAddress address = new InternetAddress(u.getEmail());
			
			message.setRecipient(Message.RecipientType.TO, address);
			message.setSubject(subject);
			message.setText(body);
			
			Transport.send(message);
			
		} catch (MessagingException e) {
			Logger.getLogger(MailEJB.class.getName()).log(Level.WARNING, "Cannot send mail", e);
		}
	}
}
