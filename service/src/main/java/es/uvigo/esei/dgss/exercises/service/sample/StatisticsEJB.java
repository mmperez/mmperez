package es.uvigo.esei.dgss.exercises.service.sample;

import javax.annotation.PostConstruct;
import javax.ejb.AccessTimeout;
import javax.ejb.ConcurrencyManagement;
import javax.ejb.ConcurrencyManagementType;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Startup
@ConcurrencyManagement(ConcurrencyManagementType.CONTAINER)
@AccessTimeout(value=5000)
@Singleton
public class StatisticsEJB {
	private long totalUser = 0;
	private long totalPost = 0;
	
	@PersistenceContext
	private EntityManager em;
	
	@PersistenceContext
    public void setEntityManager(EntityManager em) {
        this.em = em;
    }
	
	@PostConstruct
	void init() {
		totalUser = em.createQuery("SELECT count(u) FROM User u", Long.class).getSingleResult();
		totalPost = em.createQuery("SELECT count(p) FROM Post p", Long.class).getSingleResult();
	}
	
	@Lock(LockType.READ)
	public long getTotalUserCount() {
		return totalUser;
	}
	
	@Lock(LockType.WRITE)
	public void addTotalUser(long add) {
		totalUser += add;
	}
	
	@Lock(LockType.READ)
	public long getTotalPostCount() {
		return totalPost;
	}
	
	@Lock(LockType.WRITE)
	public void addTotalPost(long add) {
		totalPost += add;
	}
	
	@Lock(LockType.WRITE)
	public void subTotalPost(long sub) {
		totalPost -= sub;
	}
	
	@Lock(LockType.WRITE)
	public void subTotalUser(long sub) {
		totalPost -= sub;
	}


}