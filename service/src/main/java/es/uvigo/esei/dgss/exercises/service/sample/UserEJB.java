package es.uvigo.esei.dgss.exercises.service.sample;


import java.util.ArrayList;
import java.util.List;
import java.sql.Date;
import javax.annotation.Resource;
import javax.annotation.security.PermitAll;
import javax.ejb.EJB;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.persistence.EntityExistsException;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;

import es.uvigo.esei.dgss.exercises.domain.Friendship;
import es.uvigo.esei.dgss.exercises.domain.User;

@Stateless
public class UserEJB {

	@PersistenceContext
	private EntityManager em;
	
	@EJB
	private StatisticsEJB statisticsEJB;

	@Resource
	SessionContext ctx;

	@PermitAll
	public List<User> findAllUsers() {
		return em.createQuery("Select u FROM User u").getResultList();
	}
	
	@PermitAll
	public SessionContext getSessionContext() {
		return ctx;
	}

	@PermitAll
	public User createUser(User u) {
		em.persist(u);
		return u;

	}

	@PermitAll
	public void deleteUser(User u) {
		em.remove(u);
	}

	@PermitAll
	public User updateUser(User u) {
		User user = em.find(User.class, u.getLogin());
		user.setName(u.getName());
		user.setLogin(u.getLogin());
		user.setPassword(u.getPassword());
		user.setPicture(u.getPicture());
		user.setRole(u.getRole());
		
		em.merge(user);
		return u;
	}
	
	@PermitAll
	public User findByLogin(String login) throws EntityNotFoundException{
		User u = (User) em.createQuery("SELECT u FROM User u WHERE u.login=:l").setParameter("l", login).getSingleResult();
		if (u == null) {
			throw new EntityNotFoundException(
						"User '" + login + "' not found."
					);
		}
		
		return u;
	}
	
	@PermitAll
	public Friendship createFriendship(String loginFrom, String loginTo) throws PersistenceException {
    	Friendship f = new Friendship();
    	f.setDate(new Date(System.currentTimeMillis()));
    	f.setUserFrom(findByLogin(loginFrom));
    	f.setUserTo(findByLogin(loginTo));
    	f.setAccepted(false);
    	
    	try{
    		em.persist(f);
    		em.flush();
    		
    		return f;
    	}
    	catch(PersistenceException e) {
    		throw new EntityExistsException("The Friendship: " + loginFrom + " and " + loginTo + " exists.");
    	}
	}
	
	@PermitAll
	public void acceptFriendship(String loginFrom, String loginTo) {
		em.createQuery("UPDATE Friendship f SET f.accepted = 1 "
				+ "WHERE f.userFromLogin=:lfrom "
				+ "AND f.userToLogin=:lto")
		.setParameter("lfrom", loginFrom).setParameter("lto", loginTo).executeUpdate();
	}
	

	@SuppressWarnings("unchecked")
	@PermitAll
	public List<Friendship> getFriendshipRequest(String login) {
		List<Friendship> requests = new ArrayList<>();
		requests.addAll(em.createQuery("SELECT f FROM Friendship f"
				+ " WHERE f.userToLogin=:l AND f.accepted=0")
				.setParameter("l", login).getResultList());
		return requests;
	}
	
	@PermitAll
	public List<User> findUsers(String param) {
		String patron = "%" + param + "%";

		return em.createQuery("SELECT u FROM User u WHERE u.login LIKE :pattern", User.class)
				.setParameter("pattern", patron).getResultList();
	}
		

}