package es.uvigo.esei.dgss.exercises.service.sample;

import java.sql.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.annotation.security.PermitAll;
import javax.ejb.EJB;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.persistence.EntityExistsException;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.ws.rs.NotAllowedException;

import es.uvigo.esei.dgss.exercises.domain.Comment;
import es.uvigo.esei.dgss.exercises.domain.Link;
import es.uvigo.esei.dgss.exercises.domain.Photo;
import es.uvigo.esei.dgss.exercises.domain.Post;
import es.uvigo.esei.dgss.exercises.domain.User;
import es.uvigo.esei.dgss.exercises.domain.Video;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import es.uvigo.esei.dgss.exercises.service.*;

@Stateless
public class PostEJB {

	@PersistenceContext
	private EntityManager em;

	@Resource
	SessionContext ctx;
	
	@EJB
	private UserEJB userEJB;
	
	@EJB
	private PostEJB postEJB;
	
	@EJB
	private StatisticsEJB statisticsEJB;
	
	@PermitAll
	public Photo createPostPhoto(String login, String photo){
    	User u = userEJB.findByLogin(login);
		Photo p = new Photo();
		p.setContent(photo);
		p.setDate(new Date(System.currentTimeMillis()));
		p.setUserPost(u);
		
		em.persist(p);
		statisticsEJB.addTotalPost(1);
		
		return p;
	}
	
	@PermitAll
	public Video createPostVideo(String login, String url){
    	User u = userEJB.findByLogin(login);
		Video v = new Video();
		v.setUrl(url);
		v.setDate(new Date(System.currentTimeMillis()));
		v.setUserPost(u);
		
		em.persist(v);
		statisticsEJB.addTotalPost(1);
		
		return v;
	}
	
	@PermitAll
	public Link createPostLink(String login, String link){
    	User u = userEJB.findByLogin(login);
		Link l = new Link();
		l.setUrl(link);
		l.setDate(new Date(System.currentTimeMillis()));
		l.setUserPost(u);
		
		em.persist(l);
		statisticsEJB.addTotalPost(1);
		
		return l;
	}
	
	@PermitAll
	public void updatePost(Post post) {
		em.merge(post);
	}
	
	public void deletePost(Post post) {
		em.remove(post);
		statisticsEJB.subTotalPost(1);
	}
	
	@PermitAll
	public List<Post> getAllPost() throws EntityNotFoundException{
		@SuppressWarnings("unchecked")
		List<Post> posts = (List<Post>)em.createQuery("SELECT p FROM Post p").getResultList();
		if ( posts == null) {
			throw new EntityNotFoundException(
						"Not posts found."
					);
		}
		return posts;
	}
	
	@PermitAll
	public List<Post> getPostsByUserLogin(String login) throws EntityNotFoundException {
		User u = userEJB.findByLogin(login);
		@SuppressWarnings("unchecked")
		List<Post> posts = (List<Post>)em.createQuery("SELECT p FROM Post p WHERE p.userPost=:u").setParameter("u", u).getResultList();

		return posts;
	}

	@PermitAll
	public void deleteAuthorPost(String login, int id) throws PersistenceException, NotAllowedException {
		Post p = findById(id);
		if(p.getUserPost().getLogin().matches(login)) {
			
		}
		else {
			throw new NotAllowedException("You are not allowed");
		}
	}
	
	@PermitAll
	public void editAuthoredPost(String login, Post post) throws NotAllowedException {
		Post p = (Post) em.createQuery("SELECT p FROM Post p WHERE p.id=:id").setParameter("id", post.getId()).getSingleResult();
		if(p.getUserPost().getLogin().matches(login)) {
			updatePost(post);
		}
		else {
			throw new NotAllowedException("You are not allowed");
		}
	}
	
	@PermitAll
	public Post findById(int id) throws EntityNotFoundException{
		Post p = em.find(Post.class, id);
		if (p == null) {
			throw new EntityNotFoundException(
						"Invalid '" + id + "' post."
					);
		}
		
		return p;
	}
	
	@PermitAll
	public void addAutenticateLikes(int id, String user)
	{
		Post p = findById(id);
		User u = userEJB.findByLogin(user);
		p.addLike(u);
	}
	
	@PermitAll
	public Comment addComment(User user, Post post, String comment){
		Comment c = new Comment();
		c.setComment(comment);
		c.setDate(new Date(System.currentTimeMillis()));
		c.setUserAndPost(user, post);
		em.persist(c);		
		return c;
	}

}